var express = require('express');
var mysql = require('mysql')

var app = express();

app.get('/api/hello', function (req, res) {
  res.send(
    "<pre>Hello World!\n" +
    "Timestamp: " + Date.now() + "\n" +
    "ENVIRONMENT: " + process.env.ENVIRONMENT + "\n" +
    "DB_HOST: " + process.env.DB_HOST + "\n" +
    JSON.stringify(req.headers, " ", 2) +
    "</pre>"
  );
});

app.get('/api/myhello', function (req, res) {
  var connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
  })

  connection.connect()

  connection.query("SELECT 'Hello MySQL!' AS result", function (err, rows, fields) {
    if (err) throw err

    console.log('The solution is: ', rows[0].result);
    res.send("<pre>" + rows[0].result + "</pre>");
  })

  connection.end()

});

app.use(function (req, res, next) {
  res.status(404).send("404 Not Found " + req.originalUrl)
})

app.listen(8080, "0.0.0.0", function () {
  console.log('Example app listening on port 8080!');
});
