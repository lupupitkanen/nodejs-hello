FROM node:14

ENV ENVIRONMENT=dev \
  DB_HOST=127.0.0.1 \
  DB_PORT=3306 \
  DB_USER=myuser \
  DB_PASSWORD=mypassword \
  DB_NAME=mydb \
  MAILGUN_HOST=api.mailgun.net \
  MAILGUN_KEY=mykey \
  MAILGUN_DOMAIN=mydomain

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app

EXPOSE 8080

CMD [ "npm", "start" ]
